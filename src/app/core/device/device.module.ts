import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DeviceService } from '@app/core/device/service';

@NgModule({
  imports: [
    CommonModule
  ],
  providers: [
    DeviceService
  ]
})
export class DeviceModule { }
