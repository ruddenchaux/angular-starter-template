import { Injectable } from '@angular/core';

@Injectable()
export class LoggerService {

  constructor() { }

  /**
   * Function log
   * @param args data to log
   */
  log(...args): void {
    console.log(...args);
  }

}
