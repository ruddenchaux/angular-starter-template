import { NgModule } from '@angular/core';
import { LoggerService } from '@app/core/logger/service';

@NgModule({
  providers: [
    LoggerService
  ]
})
export class LoggerModule { }
