import { Component, OnInit } from '@angular/core';
import { BaseFeatureComponent } from '@app/shared';
import { LoggerService } from '@app/core/logger/service';

@Component({
  selector: 'app-list-data',
  templateUrl: './list-data.component.html',
  styleUrls: ['./list-data.component.scss']
})
export class ListDataComponent extends BaseFeatureComponent {

  constructor(private logger: LoggerService) {
    super();
  }

  ngOnInit() {
    this.logger.log('list');
  }

}
