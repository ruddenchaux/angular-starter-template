import { NgModule } from '@angular/core';
import { CoreModule } from '@app/core';
import { ListDataComponent } from '@app/features/a/components';

@NgModule({
  imports: [
    CoreModule
  ],
  declarations: [ListDataComponent],
  exports: [ListDataComponent]
})
export class FeatureAModule { }
