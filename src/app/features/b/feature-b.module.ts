import { NgModule } from '@angular/core';
import { CoreModule } from '@app/core';
import { DetailDataComponent } from '@app/features/b/components';

@NgModule({
  imports: [
    CoreModule
  ],
  declarations: [DetailDataComponent],
  exports: [DetailDataComponent]
})
export class FeatureBModule { }
