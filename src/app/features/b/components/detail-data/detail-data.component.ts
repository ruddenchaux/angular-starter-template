import { Component, OnInit } from '@angular/core';
import { BaseFeatureComponent } from '@app/shared';
import { LoggerService } from '@app/core/logger/service';

@Component({
  selector: 'app-detail-data',
  templateUrl: './detail-data.component.html',
  styleUrls: ['./detail-data.component.scss']
})
export class DetailDataComponent extends BaseFeatureComponent {

  constructor(private logger: LoggerService) {
    super();
  }

  ngOnInit() {
    this.logger.log('detail');
  }

}
