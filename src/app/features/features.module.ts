import { NgModule } from '@angular/core';
import { FeatureAModule } from '@app/features/a';
import { FeatureBModule } from '@app/features/b';

@NgModule({
  imports: [
    FeatureAModule,
    FeatureBModule
  ],
  exports: [
    FeatureAModule,
    FeatureBModule
  ]
})
export class FeaturesModule { }
