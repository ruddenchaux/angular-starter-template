import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BaseFeatureComponent } from '@app/shared/components';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    BaseFeatureComponent
  ],
  exports: [
    CommonModule
  ]
})
export class SharedModule { }
