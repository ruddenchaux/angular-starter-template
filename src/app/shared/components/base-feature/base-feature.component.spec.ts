import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BaseFeatureComponent } from './base-feature.component';

describe('BaseFeatureComponent', () => {
  let component: BaseFeatureComponent;
  let fixture: ComponentFixture<BaseFeatureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BaseFeatureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BaseFeatureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
